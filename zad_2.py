#Zadatak 2

while True:
  try:
    ocjena = float(raw_input("Unesite ocjenu: "))           #Korisnicki unos broja koji se pretvara u float
  except ValueError:                                        #Ako korisnicki unos nije broj, ispisuje se sljedece...
    print "Unesen string, trazi se brojcana vrijednost!"
    if True:
      break
  if ocjena >= 0 and ocjena <= 1:           #[0, 1]
    if ocjena >= 0.9:                       #slijede zadani uvjeti...
      print 'A'
    elif ocjena >= 0.8:                     #Koristi se elif kako bi se ispisao samo rezultat jednog uvjeta...
      print 'B'
    elif ocjena >= 0.7:
      print 'C'
    elif ocjena >= 0.6:
      print 'D'
    elif ocjena < 0.6:
      print 'F'
  else:
    print "Unesena je vrijednost izvan dozvoljenog intervala [0.0, 1.0]"
  break


    
#U slucaju da je broj veci od 1 ili manji od 0, ispisuje se gornja poruka...
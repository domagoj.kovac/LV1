#Zadatak 4

V = []                                          #Priprema liste...

def num_list(A, V):                             #Definiranje funkcije za upis elemenata u listu...
  V.append(A)  

while True:                                     #Unos stringa u beskonačnu petlju dok string nije 'Done'...
  try:
    A = raw_input("Unos broja: ")
    if A == 'Done':
      break
    else:
      A = float(A)                              #String u float
      num_list(A, V)                            #Poziv funkcije za upis elemenata u listu
    
  except ValueError:                            #Greška ako se unese slovo
    print "Unesena vrijednost nije valjana!"
    
L = len(V)                                      #Duljina liste...
if L > 0 and L <= 4:                            #Provjera za gramaticki ispravan upis rjesenja...
  X = 'broja'
else:
  X = 'brojeva'  

#Srednja vrijednost ispisuje se s dvje decimalne točke u obliku stringa...

print "Uneseno je" + ' ' + str(L) + ' ' + X
print "Srednja je vrijednost brojeva" + ' ' + format((sum(V)/L), '.2f') + ',' + ' ' + "najmanja je vrijednost" + ' ' + str(min(V)) + ',' + " a najveća" + ' ' + str(max(V)) + '.' 
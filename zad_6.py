#Zadatak 6

import re

D = dict()
V = []
target = "From: "

#Preciznije je koristiti 'From: ' nego 'From', jer se u suprtnom ista email adresa javlja dva puta...

fname = raw_input("Ime datoteke: ")

with open(fname) as fhand:
  for line in fhand:
    if line.find(target) == 0:
      emails = re.findall(r'[\w\.-]+@[\w\.-]+', line)       #Pronađi sve linije s emailovima...
      for email in emails:
        V.append(email)                                     #Dodaj email u niz...
        hostnames = re.findall('@[\w\.-]+', email)          #Pronađi hostname...
        for hostname in hostnames:
          if hostname not in D:                             #Pohranjivanje redom u dictionary...
            D[hostname] = 1
          else:
            D[hostname] = D[hostname] + 1
            
for i in D:
  print str(i[:]) + ' --- ' + str(D[i])
for i in V:
  print(i)